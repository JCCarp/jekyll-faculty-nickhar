---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I am an associate professor of computer science at the University of British Columbia and Canada Research Chair in Algorithm Design.


## Research Interests

Theory of machine learning, randomized algorithms, convex optimization

## Selected Publications

{% bibliography --file selected.bib %}
